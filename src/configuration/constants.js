module.exports = {
  reasonCodes: {
    NOT_SUCCESSFULL_STATUS_CODE_FROM_BINANCE: 'BinanceNotAccepted',
    EXCEPTION_TRYING_CONNECTING_TO_BINANCE: 'BinanceNotReachable',
    RECEIVED_INVALID_MESSAGE: 'ReceivedInvalidMessage',
    DISCONNECTED_FROM_SERVICE: 'DisconnectedFromService',
    STREAM_COMPLETED: 'BinanceStreamStopped',
    STREAM_STARTED: 'BinanceStreamStarted'
  },
  communicationTopology: {
    TOPIC_MARKET_ORDER: 'SagaTaskOrder',
    ROUTING_KEY_UPDATE: 'update',
    ROUTING_KEY_STOP_BINANCE: 'binance.stop',
    ROUTING_KEY_START_BINANCE: 'binance.start',
    QUEUE_BINANCE_HANDLER: 'MarketOrder_BinanceHandler'
  }
}
