module.exports = {
  proxyConnStr: process.env.HTTPS_PROXY || null,
  devLogger: process.env.DEV_LOGGER || true,
  binanceWsAPI: process.env.BINANCE_WS_API || 'wss://stream.binance.com:9443',
  wssPort: process.env.PORT || '8888',
  wssHost: process.env.WSS_HOST || 'localhost'
}
