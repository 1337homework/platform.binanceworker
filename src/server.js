require('./utils/env')
const rabbitmq = require('./interfaces/rabbit')
const websockets = require('./interfaces/websockets')
const { binanceWorkerHandler } = require('./domain/services/binanceWorkerService')
const { logger } = require('./utils/logger')
const { publishStreamStatus } = require('./utils/asyncHandlers')

const exitHandler = () => {
  websockets.closeSocketServer()
  publishStreamStatus('Terminated')
  setImmediate(() => process.exit(1))
}

process.on('SIGTERM', exitHandler)
process.on('SIGINT', exitHandler)

rabbitmq
  .openConnection()
  .then(() => {
    rabbitmq.setConsumer(binanceWorkerHandler)
    websockets.runSocketServer()
    logger.log({ level: 'info', message: 'Binance Worker Connected to RabbitMQ. Listening for commands...' })
  })
  .catch((err) => {
    logger.log({ level: 'error', message: `${err.message} - ${err.stack}` })
    process.exit(1)
  })
