const rabbitmq = require('../interfaces/rabbit')
const { logger } = require('./logger')

const handleError = (reasonCode, reason, message = {}, innerIssue, originalMessage) => {
  rabbitmq.publishStatusUpdate({
    ...message,
    status: 'Error',
    reasonCode,
    reason,
    details: {
      originalMessage,
      innerIssue
    }
  })

  rabbitmq.ack(originalMessage)

  logger.log({ level: 'error', message: reason })
}

const publishStreamStatus = (status, message = {}, originalMessage) => {
  rabbitmq.publishStatusUpdate({
    ...message,
    status,
    details: {
      originalMessage
    }
  })

  rabbitmq.ack(originalMessage)

  logger.log({
    level: 'info',
    message: `Binance stream worker status updated to ${status}`
  })
}

module.exports = {
  handleError,
  publishStreamStatus
}
