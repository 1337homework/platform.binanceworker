const WebSocket = require('ws')
const websockets = require('../../interfaces/websockets')
const generalConfig = require('../../configuration/general')
const { DISCONNECTED_FROM_SERVICE } = require('../../configuration/constants')
const { logger } = require('../../utils/logger')
const { handleError } = require('../../utils/asyncHandlers')

let client = null

const disconnect = () => {
  if (client) {
    client.close()
  }
}

const connect = () => {
  disconnect()
  const ws = websockets.getServer()

  let url = `${generalConfig.binanceWsAPI}/stream?streams=btcusdt@miniTicker/ethusdt@miniTicker/xrpusdt@miniTicker`

  if (generalConfig.proxyConnStr) {
    let HttpsProxyAgent = require('https-proxy-agent')
    let agent = new HttpsProxyAgent(generalConfig.proxyConnStr)
    client = new WebSocket(url, { agent })
  } else {
    client = new WebSocket(url)
  }

  client.on('open', () => {
    logger.log({ level: 'info', message: `Worker just connected to Binance WS API Service...` })
  })

  client.on('error', (err) => {
    logger.log({ level: 'error', message: `Binance WS Service error ${err}` })
    handleError(
      DISCONNECTED_FROM_SERVICE,
      `Error in Binance Websocket Service clientId: ${ws.clientId}`,
      {},
      err
    )
  })

  client.on('close', (err) => {
    logger.log({ level: 'info', message: `Binance WS API Service closed with ${err}` })
  })

  client.on('message', (msg) => {
    let messageObj = JSON.parse(msg)
    messageObj.marketName = 'binance'
    ws.broadcast(messageObj)
  })
}

module.exports = {
  connect,
  disconnect
}
