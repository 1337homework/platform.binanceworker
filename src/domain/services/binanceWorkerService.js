const { logger } = require('../../utils/logger')
const { handleError, publishStreamStatus } = require('../../utils/asyncHandlers')
const {
  RECEIVED_INVALID_MESSAGE,
  STREAM_COMPLETED,
  STREAM_STARTED
} = require('../../configuration/constants').reasonCodes
const {
  ROUTING_KEY_STOP_BINANCE,
  ROUTING_KEY_START_BINANCE
} = require('../../configuration/constants').communicationTopology
const binance = require('../entities/binance')

const binanceWorkerHandler = (originalMessage) => {
  let message

  try {
    message = JSON.parse(originalMessage.content.toString('utf8'))
  } catch (e) {
    handleError(
      RECEIVED_INVALID_MESSAGE,
      'Message can not be parsed',
      {},
      e,
      originalMessage
    )
    return
  }

  if (
    !message.sagaId ||
    !message.resourceId
  ) {
    handleError(
      RECEIVED_INVALID_MESSAGE,
      'Error in input. Missing resourceId, sagaId or other fields.',
      message,
      originalMessage
    )
    return
  }

  switch (originalMessage.fields.routingKey) {
  case ROUTING_KEY_START_BINANCE:
    logger.log({
      level: 'info',
      message: `Received command to start Binance stream.`
    })

    binance.connect()
    publishStreamStatus(STREAM_STARTED, message, originalMessage)
    break
  case ROUTING_KEY_STOP_BINANCE:
    logger.log({
      level: 'info',
      message: `Received command to stop Binance stream.`
    })

    binance.disconnect()
    publishStreamStatus(STREAM_COMPLETED, message, originalMessage)
    break
  default:
    handleError(
      RECEIVED_INVALID_MESSAGE,
      `Message routing key not recognized: ${originalMessage.fields.routingKey}`,
      message,
      {},
      originalMessage
    )
    break
  }
}

module.exports = {
  binanceWorkerHandler
}
